@extends('layouts.app')
@section('content')

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login form -->
                {{Form::open(['route'=>'login.store','method'=>'POST','id'=>'LoginUsr', 'class'=>'login-form'])}}
                
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <img src="img/avatar.png" alt="cargando.."  class="rounded-circle  img-responsive">
                                <h5 class="mb-0">Login</h5>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                 {!!Form::email('no_email',null,['class'=>'form-control','id'=>'no_email','placeholder'=>'ejemplo@ejemplo.com','required'=>'true','autofocus'=>'true'])!!}
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                {!!Form::password('password',['class'=>'form-control','id'=>'password','placeholder'=>'Contraseña','required'=>'true'])!!}
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-block">Continuar <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                            <div class="text-center">
                                <a href="redirect">Recuperar contraseña</a>
                            </div>
                        </div>
                    </div>
                {{Form::close()}}
                <!-- /login form -->

            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="navbar navbar-expand-lg ">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        <!--Footer-->
                    </button>
                </div>

            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
   
@endsection