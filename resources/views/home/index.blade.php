@extends('templates/layoutlte')
@section('content')

	<div class="row mt-4">
		<div class="col-md-12">

			<div class="card">
				<div class="card-header header-elements-inline">
				
					<h5 class="card-title">{{$descripcion}}:</h5>
					<div class="header-elements">
						<div class="list-icons">
	                		<a class="list-icons-item" data-action="reload"></a>
	                	</div>
                	</div>
				
				</div>
				
				<div class="card-body">

					<form id="comercial"> 

						<div class="row d-flex justify-content-around">
							<div class="col-md-8">
								<div class="form-group row">
									<label class="col-lg-3 col-form-label"><b>Periodo</b> <span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<div class="input-daterange input-group text-success" id="datepicker">

								          	<i class="icon-calendar"></i><span>&nbsp; </span><b> Desde:</b> 
								          	
								          	<input type="text" class="form-control" id="desde" name="desde" placeholder="Ingresar Fecha" style="background:darkgray;border-radius: 25px;"/>
								          	<i class="icon-calendar"></i><span>&nbsp; </span><b> Hasta:</b> 
								          	<input type="text" class="form-control" id="hasta" name="hasta" placeholder="Ingresar Fecha" style="background:darkgray;border-radius: 25px;"/>
								          
								        </div>
								        <div class="input-group date">
											 
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label"><b>Consultores</b> <span class="text-danger">*</span></label>
									<div class="col-lg-9" style="background:darkgray;border-radius: 25px;">
		                            <select   data-placeholder="Seleccione Consultores..." id="co_usuario" name="co_usuario[]" class=" select-search" multiple="multiple" data-fouc required>
		                            	@foreach ($listaConsultores as $key => $consultores) 
		                            	
											<option value="">Seleccione un Consultor</option>
											
											<option value="{{ @$consultores->rCousuario->co_usuario }}">{{ @$consultores->rCousuario->no_usuario }}</span></option>
										
										@endforeach 
										
		                            </select>
									</div>
								</div>							
							</div>
							<div class="col-xs-12 col-md-12 col-lg-2 mb-2">
								<div class="row text-center">
									<div class=" col-xs-12 col-md-4 col-lg-12 mb-1">
										<a href="#" class="btn btn-info form-control" id="relatorio" onclick="Relatorio('comercial');" style="border-radius: 25px;"> Relatório</a>
									</div>
									<div class=" col-xs-12 col-md-4 col-lg-12 mb-1">
										<a href="#" id="barra" class="btn btn-info form-control" id="grafico1" onclick="GraficoBarras('comercial');" style="border-radius: 25px;"> Grafico</a>
									</div>
									<div class=" col-xs-12 col-md-4 col-lg-12 mb-1">
										<a href="#" id="dona" class="btn btn-info form-control" onclick="GraficoDone('comercial');" style="border-radius: 25px;"> Pizza</a>
									</div>									
									
									
								</div>
							</div>
							
						</div>
						
						<!-- Seccion relatorio -->
						<div class="row">
							<div class="col-md-12" id="resultado">
								<!-- imprimiendo resultado -->
							</div>
						</div>

						<!-- fin sccion relatorio-->							
	
						<br><br>
						<div class="row">
							
							<div class="col-md-12" id="graficobarra1" style="display: none;">
								
								<canvas id="mixed-chart" width="600" height="350"></canvas>
							</div>
							<div class="col-md-12" id="graficodona" style="display: none;">
								<canvas id="pie-chart" width="800" height="450"></canvas>

							
							</div>
							
						</div>	

					</form>
				</div>
			</div>
		</div>
	</div>
@stop
<!-- Resources -->


<script src="{{asset('js/graficas.js')}}"></script>





