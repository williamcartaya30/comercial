<?php

namespace App\Http\Controllers;

use App\Models\CaoFactura;
use App\Models\CaoUsuario;
use App\Models\PermissaoSistema;
use App\Models\CaoOs;
use Illuminate\Http\Request;
use Session;
use Auth;
use Laracasts\Flash\Flash;
use Alert;
use Illuminate\Support\Facades\DB;

class ComercialController extends Controller
{

    public function index()
    {
       
        $listaConsultores=PermissaoSistema::where('co_sistema',1)->where('in_ativo','S')->whereIn('co_tipo_usuario',[0,1,2])->with('rCousuario')->get();
            
        $title="Performance Comercial";
        $descripcion="Consultores";
            
        return view('home.index', compact('title','descripcion','listaConsultores'));

    }


    public function relatorio(Request $request)
    {
        $fdesde=strtotime(date('Y-m',strtotime($request->desde)));
        $fhasta=strtotime(date('Y-m',strtotime($request->hasta)));
        
        $resultRelatorio=CaoUsuario::whereIn('co_usuario',$request->co_usuario)->with('rCaoos.rCaoFactura')->with('rCaoSalario')->get();
                                    
            $html='<table class="col-md-12 table table-striped table-bordered" >';
               
                // iterando consultores   
                foreach ($resultRelatorio as $key1 => $consultor) 
                {   
                    
                    $html.="<tr style='color:white;'><td colspan='5' bgcolor='#0e665f'> Consultor <span><i class='icon-user'></i></span>: <b>".$consultor->no_usuario."</b></td></tr>";

                     
                     $html.="<tr style='font-weight:bold;'><td>Periodo</td><td>Receita Líquida</td><td>Custo Fixo</td><td>Comissão</td><td>Lucro</td></tr>";
                          
                        $facturas=$this->ordenesServicio($consultor->rCaoos);

                        // variables para almacenar el balance total por periodo 
                        $saldo_ingreso_neto=0;  
                        $saldo_comision=0;
                        $saldo_lucro=0;
                        $saldo_costo_fijo=0;

                        // agrupa la coleccion de facturas y ordena por fechas y simplifica la dimension
                        $facturas=collect(@$facturas)->flatten(1)->sortBy('data_emissao')->groupBy('data_emissao');

                        // iterando las facturas por mes
                        foreach ($facturas as $fecha => $factura) 
                        {

                            $cantidadFacturasMes=count($factura);// cantidad de facturas existente por mes
                           
                            //variables para almacenar totales por mes
                            $suma_ingreso_neto=0;  
                            $suma_comision=0;
                            $suma_lucro=0;

                            // filtrar la fecha del periodo
                            if (strtotime($fecha)>=$fdesde && strtotime($fecha)<=$fhasta) 
                            {

                                for ($i=0; $i<$cantidadFacturasMes ; $i++) 
                                {
                                    $fecha=$factura[$i]['data_emissao'];
                                    
                                    //variables de Factura
                                    $porcentaje          = $factura[$i]['valor'] * $factura[$i]['total_imp_inc']/100;
                                    $porcentaje_comision = $factura[$i]['comissao_cn'] / 100;
                                    $neto                = $factura[$i]['valor'] - $porcentaje;
                                    $costoFijo           = @$consultor->rCaoSalario->brut_salario;
                                    $comision            = ($factura[$i]['valor'] - $porcentaje)*$porcentaje_comision;
                                    
                                    $suma_ingreso_neto+=$neto;
                                    $suma_comision+=$comision;                               

                                }

                                //variables de saldo 
                                $suma_lucro=$suma_ingreso_neto-($costoFijo+$suma_comision);
                                $saldo_ingreso_neto+=$suma_ingreso_neto;
                                $saldo_comision+=$suma_comision;
                                $saldo_lucro+=$suma_lucro;
                                $saldo_costo_fijo+=$costoFijo;

                                //impresion de filas con la sumatoria por mes html    
                                $html.="<tr>";
                                    $html.="<td>".$fecha."</td>";
                                    $html.="<td> R$ ".number_format( $suma_ingreso_neto, 2, ',', '.' )."</td>";
                                    $html.="<td> R$ ".number_format( $costoFijo, 2, ',', '.' )."</td>";
                                    $html.="<td> R$ ".number_format( $suma_comision, 2, ',', '.' )."</td>";
                                    $html.="<td> R$ ".number_format( $suma_lucro, 2, ',', '.' )."</td>";
                                $html.="</tr>";

                            }
                        }

                    //impresion de filas con el balance por periodo html  
                    $html.="<tr style='background:#dbfaf8'>";
                        $html.="<td ><b>Saldo:</b></td>";
                        $html.="<td> R$ ".number_format( $saldo_ingreso_neto, 2, ',', '.' )."</td>";
                        $html.="<td> R$ ".number_format( $saldo_costo_fijo, 2, ',', '.' )."</td>";
                        $html.="<td> R$ ".number_format( $saldo_comision, 2, ',', '.' )."</td>";
                        $html.="<td> R$ ".number_format( $saldo_lucro, 2, ',', '.' )."</td>";
                    $html.="</tr>";
                }

            $html.="</table>";

        return $html;
    }

    public function ordenesServicio($ordenes)
    {
        // Iterando ordenes de servicios para extraer las facturas  
        foreach ($ordenes as $key => $ordenServicio) 
        {
                // Procesando solo ordenes de servicios con facturas
                if (!empty($ordenServicio->rCaoFactura->get(0)->valor)) 
                { 
                    $procesarfacturas[]=$this->procesarFacturas($ordenServicio->rCaoFactura);
                }
        }

        // Si no tiene facturas retornamos 0
        if (empty($procesarfacturas)) {
            
            $procesarfacturas=0;
        }

         return $procesarfacturas;
                    
    }

    public function procesarFacturas($listaFacturas)
    {
            // Iterando las facturas 
            foreach ($listaFacturas as $key => $factura) 
            {
                    // creando la colecion de facturas y transformando las fechas 
                    $facturas[]=[                                            
                                    'data_emissao'  => date('Y-m',strtotime($factura->data_emissao)),
                                    'co_fatura'     => $factura->co_fatura,
                                    'co_cliente'    => $factura->co_cliente,
                                    'co_sistema'    => $factura->co_sistema,
                                    'co_os'         => $factura->co_os,
                                    'total'         => $factura->total,
                                    'valor'         => $factura->valor,
                                    'comissao_cn'   => $factura->comissao_cn,
                                    'total_imp_inc' => $factura->total_imp_inc 
                                ];                
            }
   
        return collect($facturas); 
            
    }

    

    public function GraficoBarras (Request $request, CaoFactura $caoFactura){

         //grafico1
        $grafico1=DB::table('cao_fatura AS cf')
            ->join('cao_os AS co','cf.co_os','=','co.co_os')
            ->join('cao_usuario AS cu','co.co_usuario','=','cu.co_usuario')


            ->select(
                DB::raw('sum(cf.valor-(cf.total_imp_inc/100)*cf.valor) AS liquido'), 
                DB::raw('MONTH(cf.data_emissao) AS mes'),
                DB::raw('cu.no_usuario')
                )
            ->whereBetween('cf.data_emissao',[$request->desde,$request->hasta])
            ->whereIn('co.co_usuario',$request->co_usuario)
            ->groupBy('mes','no_usuario')
            ->get();
 
            $grafico1=collect($grafico1)->groupBy('no_usuario');
           
        return response()->json($grafico1);
        
        

    }


    public function GraficoDone (Request $request, CaoFactura $caoFactura){

        $grafico2=DB::table('cao_fatura AS cf')
            ->join('cao_os AS co','cf.co_os','=','co.co_os')
            ->join('cao_usuario AS cu','co.co_usuario','=','cu.co_usuario')


            ->select(
                DB::raw('sum(cf.valor-(cf.total_imp_inc/100)*cf.valor) AS liquido'), 
                DB::raw('cu.no_usuario')
                
                )
            ->whereBetween('cf.data_emissao',[$request->desde,$request->hasta])
            ->whereIn('co.co_usuario',$request->co_usuario)
            ->groupBy('no_usuario')
            ->get();

        return response()->json($grafico2);
 

    }
}
