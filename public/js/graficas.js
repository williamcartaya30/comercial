function Relatorio(idForm) 
{
	if ($("#desde").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Desde', 'warning',{ button: "Ok!",});
                $("#desde").focus();
	}
	if ($("#hasta").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Hasta', 'warning',{ button: "Ok!",});
                $("#hasta").focus();
	}
	if ($("#co_usuario").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe Seleccionar un consultor', 'warning',{ button: "Ok!",});
                $("#co_usuario").focus();
	}

	$('#resultado').show(1000).animate({width: "show"}, 1000,"linear");
  	$('#graficobarra1,#graficodona').hide(1000).animate({height: "hide"}, 1000,"linear");
		var formData = $("#" + idForm).serialize();

		// paticion ajax
        var request = $.ajax({                        
					           type: "GET",                 
					           url: "/admin/comercial/relatorio",                     
					           data: formData,
					           //dataType: "html"
						      });
        // validando respuesta 
        request.done(function( data ) {
				
			  	$('#resultado').html(data)
		});

        // falla la respuesta
        request.fail(function( jqXHR, textStatus ) {
			  data="<div class='col-md-12 text-danger'> No se Encontro Resultados...</div>";
			  $('#resultado').html(data);
		});

	return false;
}



function GraficoBarras(idForm) {
	
	if ($("#desde").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Desde', 'warning',{ button: "Ok!",});
                $("#desde").focus();
	}
	if ($("#hasta").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Hasta', 'warning',{ button: "Ok!",});
                $("#hasta").focus();
	}
	if ($("#co_usuario").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe Seleccionar un consultor', 'warning',{ button: "Ok!",});
                $("#co_usuario").focus();
	}
	$('#graficobarra1').show(1000).animate({width: "show"}, 1000,"linear");
  	$('#resultado,#graficodona').hide(1000).animate({height: "hide"}, 1000,"linear");

    var formData = $("#" + idForm).serialize(); 

    // declarando el objeto con los parametros del grafico 
    var options={
				    type: 'bar',
				    data: {
					      labels: [],
					      datasets:[]
						    },
				    options: {
				      title: {
				        display: true,
				        text: 'Desempeño de Consultores'
				      },
				      legend: { display: true }
				    }
				};
    

    	// paticion ajax
        var request = $.ajax({ type: "GET",  url: "/admin/GraficoBarras",   data: formData,});

        // respuesta 
	        request.done(function( result ) 
	        {
	        	var meses=[];
					
				//  iterando para optener todo los consultores y sus valores	
					$.each(result, function(nombres,values)
					{
						var color= "#1c0000".replace(/0/g, function () {return (~~(Math.random() * 16)).toString(16);});
					    
					    // declarando el objeto datasets con los datos iniciales (nombre y color de barras)  
						var objetoDatasets={ label: nombres,  type: "bar",  backgroundColor: color, data: [] };

								//iterando los datos por cada consultor
								$.each(values, function(index,datos)
								{	
									
										// agregando todos los valores (Netos) en el objeto
										objetoDatasets.data.push(datos.liquido.toFixed(2));
										
										// se obtiene los meses
										meses[datos.mes]=0;

								});

						// se agrega esos valores en el objeto options de la grafica
						options.data.datasets.push(objetoDatasets);
						
					});

					// iteramos los meses agrupados
					$.each(meses, function(mes,values)
					{
						if(mes>0)
						{
							// obtenemos el nombre del mes
							var fecha=mes+'/01/2019',dates=new Date(fecha),locale="es-us",month=dates.toLocaleString(locale,{month:"long"});

							// agregamos los meses en el objeto de opciones de la grafica
							options.data.labels.push(month);

						}
						
					});
				
					// instanciamos la grafica con los parametros
					new Chart(document.getElementById("mixed-chart"), options );
				  	
			});

        // falla la respuesta
        request.fail(function( jqXHR, textStatus ) {
			  result="<div class='col-md-12 text-danger'> No se pudo procesar su solicitud...</div>";
			  $('#resultado').html(result);
		});
    
    return false;
}



function GraficoDone(idForm) 
{
	if ($("#desde").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Desde', 'warning',{ button: "Ok!",});
                $("#desde").focus();
	}
	if ($("#hasta").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Hasta', 'warning',{ button: "Ok!",});
                $("#hasta").focus();
	}
	if ($("#co_usuario").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe Seleccionar un consultor', 'warning',{ button: "Ok!",});
                $("#co_usuario").focus();
	}

	$('#graficodona').show(1000).animate({width: "show"}, 1000,"linear");
  	$('#graficobarra1,#resultado').hide(1000).animate({height: "hide"}, 1000,"linear");

		var formData = $("#" + idForm).serialize();
		var options={
					    type: 'pie',
					    data: {
						      labels: [],
						      datasets: [{
							        label: "Porcentaje % de Ganancias Netas",
							        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
							        data: []
						        }]
							  },
					    options: {
					      		title: {
							        display: true,
							        text: 'Porcentaje % de Ganancias Netas'
							      }
			    		}
					};

	
        var request = $.ajax({ type: "GET",  url: "/admin/GraficoDone", data: formData });
   
        request.done(function( result ) 
        {
				
				$.each(result, function(index,values){
					
					options.data.labels.push(values.no_usuario);
					options.data.datasets[0].data.push(values.liquido.toFixed(2));
					
				});
				
				new Chart(document.getElementById("pie-chart"), options);
			  	
		});

        request.fail(function( jqXHR, textStatus ) {
			  result="<div class='col-md-12 text-danger'> No se Encontro Resultados...</div>";
			  $('#resultado').html(result);
		});

		
	
	return false;
}